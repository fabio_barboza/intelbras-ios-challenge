//
//  NewsCell.swift
//  News
//
//  Created by Fabio Barboza de Oliveira on 21/10/18.
//  Copyright © 2018 Fabio Barboza de Oliveira. All rights reserved.
//

import Foundation
import UIKit

class NewsCell: UITableViewCell
{
    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var newsAuthors: UILabel!
    @IBOutlet weak var newsDate: UILabel!
}
