//
//  UIIMageViewExtension.swift
//  News
//
//  Created by Fabio Barboza de Oliveira on 21/10/18.
//  Copyright © 2018 Fabio Barboza de Oliveira. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
