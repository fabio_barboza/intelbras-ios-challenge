//
//  NewsTag.swift
//  News
//
//  Created by Fabio Barboza de Oliveira on 21/10/18.
//  Copyright © 2018 Fabio Barboza de Oliveira. All rights reserved.
//

import Foundation

class NewsTag: Codable {
    var id: Int
    var label: String
}
