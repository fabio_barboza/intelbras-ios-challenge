//
//  NewsDetailViewController.swift
//  News
//
//  Created by Fabio Barboza de Oliveira on 21/10/18.
//  Copyright © 2018 Fabio Barboza de Oliveira. All rights reserved.
//

import Foundation
import UIKit

class NewsDetailViewController: UIViewController {
    
    var news: News!
    
    @IBOutlet weak var newsAuthors: UILabel!
    @IBOutlet weak var newsDate: UILabel!
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var newsContent: UILabel!
    @IBOutlet weak var newsSource: UILabel!
    @IBOutlet weak var newsTags: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //let imageUrl = URL(string: news.image_url)
        //newsImage?.load(url: imageUrl!)
        newsImage.sd_setImage(with: URL(string: news.image_url))
        newsAuthors.text = news.authors
        newsDate.text = news.date
        newsTitle.text = news.title
        newsContent.text = news.content
        newsSource.text = "Source: \(news.website)"
        newsTags.text = "Tags: \(news.tags[0].label)"
    }
    
}
