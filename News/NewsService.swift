//
//  NewsService.swift
//  News
//
//  Created by Fabio Barboza de Oliveira on 21/10/18.
//  Copyright © 2018 Fabio Barboza de Oliveira. All rights reserved.
//

import Foundation

enum NewsError {
    case url
    case taskError(error: Error)
    case noResponse
    case noData
    case responseStatusCode(code: Int)
    case invalidJSON
}

class NewsService {
    private static let basePath = "http://ec2-34-215-199-111.us-west-2.compute.amazonaws.com:5000/desafio"
    
    private static let configuration: URLSessionConfiguration = {
        let config = URLSessionConfiguration.default
        config.allowsCellularAccess = true
        config.httpAdditionalHeaders = ["Content-Type": "application/json"]
        config.timeoutIntervalForRequest = 60.0
        config.httpMaximumConnectionsPerHost = 10
        return config
    }()
    
    private static let session = URLSession(configuration: configuration)
    
    class func loadNews(onComplete: @escaping ([News]) -> Void, onError: @escaping (NewsError) -> Void) {
        guard let url = URL(string: basePath) else {
            onError(.url)
            return
        }
        
        let dataTask = session.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) in
            
            if (error == nil) {
                guard let response = response as? HTTPURLResponse else {
                    onError(.noResponse)
                    return
                }
                if response.statusCode == 200 {
                    guard let data = data else {return}
                    do {
                        let newsList = try JSONDecoder().decode([News].self, from: data)
                        onComplete(newsList)
                    } catch {
                        onError(.invalidJSON)
                    }
                } else {
                    onError(.responseStatusCode(code: response.statusCode))
                }
            } else {
                onError(.taskError(error: error!))
            }
            
        }
        dataTask.resume()
    }
}
