//
//  ViewController.swift
//  News
//
//  Created by Fabio Barboza de Oliveira on 21/10/18.
//  Copyright © 2018 Fabio Barboza de Oliveira. All rights reserved.
//

import UIKit
import SDWebImage

class ViewController: UITableViewController {
    
    private var newsList: [News] = []
    
    private func loadData() {
        //Only load if the list is empty
        if (self.newsList.count == 0) {
            NewsService.loadNews(onComplete: { (newsList) in
                self.newsList = newsList
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }) { (error) in
                let alertaController = UIAlertController(title: "Error", message: "Internet connection error", preferredStyle: .alert)
                
                let acaoConfirmnar = UIAlertAction(title: "Ok", style: .default, handler: nil)
                
                alertaController.addAction(acaoConfirmnar)
                
                self.present(alertaController, animated: true, completion: nil)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        loadData()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.newsList.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let news = newsList[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "idReusableCell", for: indexPath) as! NewsCell
        
        cell.newsTitle?.text = news.title
        cell.newsAuthors?.text = news.authors
        cell.newsDate?.text = news.date
        //let imageUrl = URL(string: news.image_url)
        cell.newsImage.layer.cornerRadius = 42
        cell.newsImage.clipsToBounds = true
        cell.newsImage.sd_setImage(with: URL(string: news.image_url))
        //cell.newsImage?.load(url: imageUrl!)
        if (UserDefaults.standard.bool(forKey: newsList[indexPath.row].title)) {
            cell.newsTitle.font = UIFont.systemFont(ofSize: 16)
        } else {
            cell.newsTitle.font = UIFont.boldSystemFont(ofSize: 16)
        }
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "idNewsDetailSegue") {
            if let indexPath = tableView.indexPathForSelectedRow {
                let news = self.newsList[indexPath.row]
                let newsDetailsController = segue.destination as! NewsDetailViewController
                newsDetailsController.news = news
            }
        }
    }
    
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        loadData()
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let cell = tableView.cellForRow(at: indexPath) as! NewsCell
        
        if (!UserDefaults.standard.bool(forKey: newsList[indexPath.row].title)) {
            UserDefaults.standard.set(true, forKey: newsList[indexPath.row].title)
            cell.newsTitle.font = UIFont.systemFont(ofSize: 16)
        }

        return indexPath
    }

    @IBAction func sort(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Sort by Author", style: .default) { _ in
            self.newsList = self.newsList.sorted(by: {$1.authors > $0.authors})
            self.tableView.reloadData()
        })
        
        alert.addAction(UIAlertAction(title: "Sort by Date", style: .default) { _ in
            self.newsList = self.newsList.sorted(by: {$1.date > $0.date})
            self.tableView.reloadData()
        })
        
        alert.addAction(UIAlertAction(title: "Sort by Title", style: .default) { _ in
            self.newsList = self.newsList.sorted(by: {$1.title > $0.title})
            self.tableView.reloadData()
        })
        
        present(alert, animated: true)
    }
}

