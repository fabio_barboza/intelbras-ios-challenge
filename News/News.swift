//
//  News.swift
//  News
//
//  Created by Fabio Barboza de Oliveira on 21/10/18.
//  Copyright © 2018 Fabio Barboza de Oliveira. All rights reserved.
//

import Foundation

class News: Codable {
    var authors: String
    var content: String
    var date: String
    var image_url: String
    var title: String
    var website: String
    var tags:[NewsTag] = []
}
