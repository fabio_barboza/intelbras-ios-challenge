#Intelbras IOS Challenge


As requested the application was created using the Swift 4 language and is fetching the provided endpoint.

## MVP

1. Create a private repository on Bitbucket and add @vitorsilveira as admin;

Done

2. Create a screen with the list of articles following the provided mockup;

Done

3. Ability to visualize the article�s content (all provided info);

Done

4. Ability to sort articles (date, title and author);

Done

5. Ability to mark articles as read/unread;

Done

6. Create a pull-request and assign it to @vitorsilveira.

Done


## Extras

- Use d dependency manager

Done - Carthage Dependency Manager

- Tablet-adaptive layout;

Done

- Persistent info;

Done

- Design tweaks: animation, icons, etc;

Done

- Manage network errors;

Done - If could not load the data the system will show an alert and if pull the list will try to reload the data.

- Image caching;

Done - SDImage Library